package com.mitocode.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the publicador_seguidor database table.
 * 
 */
@Entity
@Table(name="publicador_seguidor")
@NamedQuery(name="PublicadorSeguidor.findAll", query="SELECT p FROM PublicadorSeguidor p")
public class PublicadorSeguidor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private Timestamp fecha;

	//bi-directional many-to-one association to Persona
	@ManyToOne
	@JoinColumn(name="id_seguidor")
	private Persona persona1;

	//bi-directional many-to-one association to Persona
	@ManyToOne
	@JoinColumn(name="id_publicador")
	private Persona persona2;

	public PublicadorSeguidor() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getFecha() {
		return this.fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public Persona getPersona1() {
		return this.persona1;
	}

	public void setPersona1(Persona persona1) {
		this.persona1 = persona1;
	}

	public Persona getPersona2() {
		return this.persona2;
	}

	public void setPersona2(Persona persona2) {
		this.persona2 = persona2;
	}

}