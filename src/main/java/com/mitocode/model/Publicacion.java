package com.mitocode.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the publicacion database table.
 * 
 */
@Entity
@NamedQuery(name="Publicacion.findAll", query="SELECT p FROM Publicacion p")
public class Publicacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String cuerpo;

	//bi-directional many-to-one association to Mencion
	@OneToMany(mappedBy="publicacion")
	private List<Mencion> mencions;

	//bi-directional many-to-one association to Persona
	@ManyToOne
	@JoinColumn(name="id_publicador")
	private Persona persona;

	//bi-directional many-to-one association to Tag
	@OneToMany(mappedBy="publicacion")
	private List<Tag> tags;

	public Publicacion() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCuerpo() {
		return this.cuerpo;
	}

	public void setCuerpo(String cuerpo) {
		this.cuerpo = cuerpo;
	}

	public List<Mencion> getMencions() {
		return this.mencions;
	}

	public void setMencions(List<Mencion> mencions) {
		this.mencions = mencions;
	}

	public Mencion addMencion(Mencion mencion) {
		getMencions().add(mencion);
		mencion.setPublicacion(this);

		return mencion;
	}

	public Mencion removeMencion(Mencion mencion) {
		getMencions().remove(mencion);
		mencion.setPublicacion(null);

		return mencion;
	}

	public Persona getPersona() {
		return this.persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public List<Tag> getTags() {
		return this.tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public Tag addTag(Tag tag) {
		getTags().add(tag);
		tag.setPublicacion(this);

		return tag;
	}

	public Tag removeTag(Tag tag) {
		getTags().remove(tag);
		tag.setPublicacion(null);

		return tag;
	}

}