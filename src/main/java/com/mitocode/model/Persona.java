package com.mitocode.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the persona database table.
 * 
 */
@Entity
@NamedQuery(name="Persona.findAll", query="SELECT p FROM Persona p")
public class Persona implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idpersona;

	private String apellidos;

	private String direccion;

	private byte[] foto;

	private String nombres;

	private String pais;

	private String sexo;

	//bi-directional many-to-one association to Publicacion
	@OneToMany(mappedBy="persona")
	private List<Publicacion> publicacions;

	//bi-directional many-to-one association to PublicadorSeguidor
	@OneToMany(mappedBy="persona1")
	private List<PublicadorSeguidor> publicadorSeguidors1;

	//bi-directional many-to-one association to PublicadorSeguidor
	@OneToMany(mappedBy="persona2")
	private List<PublicadorSeguidor> publicadorSeguidors2;

	//bi-directional one-to-one association to Usuario
	@OneToOne(mappedBy="persona")
	private Usuario usuario;

	public Persona() {
	}

	public Integer getIdpersona() {
		return this.idpersona;
	}

	public void setIdpersona(Integer idpersona) {
		this.idpersona = idpersona;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public byte[] getFoto() {
		return this.foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	public String getNombres() {
		return this.nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getPais() {
		return this.pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getSexo() {
		return this.sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public List<Publicacion> getPublicacions() {
		return this.publicacions;
	}

	public void setPublicacions(List<Publicacion> publicacions) {
		this.publicacions = publicacions;
	}

	public Publicacion addPublicacion(Publicacion publicacion) {
		getPublicacions().add(publicacion);
		publicacion.setPersona(this);

		return publicacion;
	}

	public Publicacion removePublicacion(Publicacion publicacion) {
		getPublicacions().remove(publicacion);
		publicacion.setPersona(null);

		return publicacion;
	}

	public List<PublicadorSeguidor> getPublicadorSeguidors1() {
		return this.publicadorSeguidors1;
	}

	public void setPublicadorSeguidors1(List<PublicadorSeguidor> publicadorSeguidors1) {
		this.publicadorSeguidors1 = publicadorSeguidors1;
	}

	public PublicadorSeguidor addPublicadorSeguidors1(PublicadorSeguidor publicadorSeguidors1) {
		getPublicadorSeguidors1().add(publicadorSeguidors1);
		publicadorSeguidors1.setPersona1(this);

		return publicadorSeguidors1;
	}

	public PublicadorSeguidor removePublicadorSeguidors1(PublicadorSeguidor publicadorSeguidors1) {
		getPublicadorSeguidors1().remove(publicadorSeguidors1);
		publicadorSeguidors1.setPersona1(null);

		return publicadorSeguidors1;
	}

	public List<PublicadorSeguidor> getPublicadorSeguidors2() {
		return this.publicadorSeguidors2;
	}

	public void setPublicadorSeguidors2(List<PublicadorSeguidor> publicadorSeguidors2) {
		this.publicadorSeguidors2 = publicadorSeguidors2;
	}

	public PublicadorSeguidor addPublicadorSeguidors2(PublicadorSeguidor publicadorSeguidors2) {
		getPublicadorSeguidors2().add(publicadorSeguidors2);
		publicadorSeguidors2.setPersona2(this);

		return publicadorSeguidors2;
	}

	public PublicadorSeguidor removePublicadorSeguidors2(PublicadorSeguidor publicadorSeguidors2) {
		getPublicadorSeguidors2().remove(publicadorSeguidors2);
		publicadorSeguidors2.setPersona2(null);

		return publicadorSeguidors2;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}